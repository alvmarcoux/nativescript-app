import { Component, OnInit } from "@angular/core";
import * as camera from "nativescript-camera";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as SocialShare from "nativescript-social-share";
import { ImageSource } from "tns-core-modules/image-source";
import * as app from "tns-core-modules/application";
import { Page, isAndroid } from "tns-core-modules/ui/page";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { Producto } from "../domain/producto.model";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html", 
    styleUrls:["./home.css"] 
})
export class HomeComponent implements OnInit {
    device: string;
    comprados: Producto[] = [];
    
    constructor(private store: Store<AppState>) {
        this.store
            .select((state) => state.productos)
            .subscribe((data) => { this.comprados = data.itemsComprados; });
        // Use the component constructor to inject providers.
    }
    
    ngOnInit(): void {
        if (isAndroid)
            this.device = "Ejecutandose en Android";
        // exports.get
        // if ()
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }


    onCamTap(): void {
        camera.requestPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                camera.takePicture(options).
                    then((imageAsset) => {
                        console.log("Tamaño: " + imageAsset.options.width + "x" + imageAsset.options.height);
                        console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatio);
                        console.log("Foto guardada!");
                        ImageSource.fromAsset(imageAsset)
                            .then((imageSource) => {
                                SocialShare.shareImage(imageSource, "Asunto: compartido desde el curso!");
                            }).catch((err) => {
                                console.log("Error -> " + err.message);
                            });
                    }).catch((err) => {
                        console.log("Error -> " + err.message);
                    });
            },
            function failure() {
                console.log("Permiso de camara no aceptado por el usuario");
            }
        );
    }    
}
