import { Component, OnInit } from "@angular/core";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    nombreUsuario: string;
    constructor(private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    doLater(fn) {setTimeout(fn, 1000);}

    ngOnInit():void {
        let LS = require( "nativescript-localstorage" ); 
        // localStorage.setItem('nombreUsuario', 'Alf'); 
        this.nombreUsuario = LS.getItem('nombreUsuario', 'A');        
    }


    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onConfigTap(): void {
        this.routerExtensions.navigate(["configVars"], {
            transition: {
                name: "fade"
            }
        });        
    }

}
