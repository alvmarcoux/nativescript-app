import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ConfigVarsComponent } from "./configVars.component";

const routes: Routes = [
    { path: "", component: ConfigVarsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ConfigVarsRoutingModule { }
