import { Component, OnInit, ElementRef } from "@angular/core";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { RouterExtensions } from "nativescript-angular/router";
import { View } from "tns-core-modules/ui/core/view";

@Component({
    moduleId: module.id, 
    selector: "ConfigVars",
    templateUrl: "./configVars.component.html"
})
export class ConfigVarsComponent implements OnInit {

    nombreUsuario: string;
    constructor(private routerExtensions: RouterExtensions) {
    }

    doLater(fn) {setTimeout(fn, 1000);}

    ngOnInit():void {
        let LS = require( "nativescript-localstorage" ); 
        this.nombreUsuario = LS.getItem('nombreUsuario');        
        // localStorage.setItem('nombreUsuario', 'Alf'); 
    }


    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onGuardarTap(): void {
        if (this.nombreUsuario.length > 0) {
            let LS = require( "nativescript-localstorage" ); 
            localStorage.setItem('nombreUsuario', this.nombreUsuario);
            console.log('n u', LS.getItem('nombreUsuario'));
            this.routerExtensions.navigate(["settings"], {
                transition: {
                    name: "fade"
                }
            });   
        }
    }
}
