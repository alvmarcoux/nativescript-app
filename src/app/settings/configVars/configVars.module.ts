import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ConfigVarsRoutingModule } from "./configVars-routing.module";
import { ConfigVarsComponent } from "./configVars.component";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

@NgModule({
    imports: [
        NativeScriptFormsModule, 
        NativeScriptCommonModule,
        ConfigVarsRoutingModule
    ],
    declarations: [
        ConfigVarsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ConfigVarsModule { }
