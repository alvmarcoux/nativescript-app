import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ProductosService } from "../domain/productos.service";

@Component({
    selector: "Search",
    moduleId: module.id, 
    templateUrl: "./search.component.html",
    // providers: [ProductosService],

})
export class SearchComponent implements OnInit {

    constructor(private _productos: ProductosService) {
        // Use the component constructor to inject providers.
    }

    public get productos() : ProductosService {
        return this._productos;
    }
    
    ngOnInit(): void {
        // console.log("hola");
        // console.dir({nombre: {nombre:{nombre: "Alfredo"}}});
        this._productos.agregar("Platano")
        this._productos.agregar("Naranja")
        this._productos.agregar("Guayaba")
        this._productos.agregar("Manzana")
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x):void {
        console.dir(x);
    }
}
