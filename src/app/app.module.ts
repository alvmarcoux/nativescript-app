import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
// import { MinLenDirective } from "./minlen.validator";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { 
    initializeProductosState,
    ProductosEffects,
    ProductosState, 
    reducersProductos } from "./domain/productos-state.model";
import { EffectsModule } from "@ngrx/effects";
import { ProductosService } from "./domain/productos.service";

// redux init
// tslint:disable-next-line:interface-name
export interface AppState{
    productos: ProductosState
}

const reducers: ActionReducerMap<AppState> = {
    productos: reducersProductos
}

const reducersInitialState  = {
    productos: initializeProductosState()
}
// fin redux init

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule, 
        NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState }), 
        EffectsModule.forRoot([ProductosEffects])
    ],
    providers: [ProductosService],
    declarations: [
        AppComponent,
        // MinLenDirective
        // ProductosComponent,
        // ClientesComponent,
        // FacturacionComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
