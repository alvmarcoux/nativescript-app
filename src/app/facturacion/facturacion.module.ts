import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { FacturacionComponent } from './facturacion.component';
import { FacturacionRoutingModule } from './facturacion-routing.module';


@NgModule({
  declarations: [FacturacionComponent],
  imports: [
    NativeScriptCommonModule, FacturacionRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class FacturacionModule { }
