import { ActivatedRoute } from "@angular/router";
import { Component, OnInit, Input } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: "DetalleProductos",
    moduleId: module.id,
    templateUrl: "./detalleProductos.component.html"
})
export class DetalleProductosComponent implements OnInit {

    // @Input() par: string;
    producto:string = "";
    private opiniones: Array<string> = [];
    constructor(private activatedroute:ActivatedRoute) {
        console.log('recibe par ' + activatedroute.snapshot.paramMap.get('par'));
        this.producto = activatedroute.snapshot.paramMap.get('par');
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.opiniones.push("Producto de calidad");
        this.opiniones.push("Buen producto");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onComprarTap(args) {
        console.log("tapped!");
        dialogs.alert({
            title: "Pedido realizado",
            message: "Se ha realizado el pedido de " + this.producto, // + " al carrito.",
            okButtonText: "Ok"
        }).then(()  => console.log("Cuadro de mensaje cerrado"))        
    }


}
