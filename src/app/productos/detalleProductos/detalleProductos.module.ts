import { DetalleProductosRoutingModule } from "./detalleProductos-routing.module";
import { DetalleProductosComponent } from "./detalleProductos.component";
import { SoloLetrasDirective } from "../../sololetras.validator";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptCommonModule } from "nativescript-angular/common";

@NgModule({
    imports: [
        NativeScriptFormsModule, 
        NativeScriptCommonModule,
        DetalleProductosRoutingModule
    ],
    declarations: [
        DetalleProductosComponent,
        SoloLetrasDirective
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class DetalleProductosModule { }
