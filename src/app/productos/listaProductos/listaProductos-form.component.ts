import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef } from "@angular/core";
import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/core/view";

@Component({
    selector: "ListaProductosForm",
    moduleId: module.id,
    template: `
    <FlexboxLayout flexDirection="row"> 
        <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar texto..."
          required minlen="4" ></TextField>
        <Label *ngIf="texto.hasError('required')" text="*"></Label>
        <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="4+"></Label>
    </FlexboxLayout>
    <Label text="(Ej: Manzana, Tomate, Datil)"></Label>
    <Button text="Buscar!" (tap)="onButtonTap()" *ngIf="texto.valid" #btnAnimado></Button>
    `
})
export class ListaProductosFormComponent implements OnInit {
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
    @Input() inicial: string;
    @ViewChild("btnAnimado", {
        read: false,  static: false
    }) btnAnimado: ElementRef;

    ngOnInit(): void {
        this.textFieldValue = this.inicial;
    }

    onButtonTap(): void {
        // console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            this.search.emit(this.textFieldValue);
            const btn = <View>this.btnAnimado.nativeElement;
            btn.animate({
                rotate: 360, 
                duration: 500, 
                delay:70            
            }).then(() => btn.animate({
                rotate: 0, 
                duration: 500, 
                delay:150            
            }));
            
            // .animate({
            //     backgroundColor: new Color("red"), 
            //     duration: 300, 
            //     delay:70
            // }).then(() => btn.animate({
            //     backgroundColor: new Color("white"), 
            //     duration: 300, 
            //     delay:150
            // }));
    
        }
    }


}