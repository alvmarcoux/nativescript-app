import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ListaProductosComponent } from "./listaProductos.component";

const routes: Routes = [
    { path: "", component: ListaProductosComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ListaProductosRoutingModule { }
