import { ListaProductosComponent } from "./listaProductos.component";
import { ListaProductosFormComponent } from "./listaProductos-form.component";
import { ListaProductosRoutingModule } from "./listaProductos-routing.module";
import { MinLenDirective } from "../../minlen.validator";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
// import { NoticiasService } from "../domain/noticias.service";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ListaProductosRoutingModule, 
        NativeScriptFormsModule
    ],
    declarations: [
        ListaProductosComponent, 
        ListaProductosFormComponent, 
        MinLenDirective
    ],
    // providers: [NoticiasService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ListaProductosModule { }
