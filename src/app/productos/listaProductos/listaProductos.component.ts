import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { GestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ProductosService } from "../../domain/productos.service";
import { RouterExtensions } from "nativescript-angular/router";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as SocialShare from "nativescript-social-share";
import * as Toast from "nativescript-toasts";
import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/core/view";
import { Image } from "tns-core-modules/ui/image";
import { Producto } from "~/app/domain/producto.model";
import { AppState } from "~/app/app.module";
import { Store } from "@ngrx/store";
@Component({
    selector: "ListaProductos",
    moduleId: module.id, 
    templateUrl: "./listaProductos.component.html",
    // providers: [ProductosService],

})
export class ListaProductosComponent implements OnInit {
    resultados: Array<Producto>;
    @ViewChild("layout", {
        read: false,  static: false
    }) layout: ElementRef;
    @ViewChild("icono", {
        read: false,  static: false
    }) icono: ElementRef;

    constructor(private _productos: ProductosService, 
        private routerExtensions: RouterExtensions, 
        private store: Store<AppState>
        ) {
        // Use the component constructor to inject providers.
    }

    public get productos() : ProductosService {
        return this._productos;
    }
    
    ngOnInit(): void {
        // this.store
        // this._productos.agregar("Platano")
        // this._productos.agregar("Naranja")
        // this._productos.agregar("Higo")
        // this._productos.agregar("Datil")
        // this._productos.agregar("Guayaba")
        // this._productos.agregar("Manzana")
        // this._productos.agregar("Tomate")
        // this._productos.agregar("Mermelada")
        // this._productos.agregar("Pera")
        // this._productos.agregar("Calabaza")
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args):void {
        var tappedView = args.view,
        tappedItem = tappedView.bindingContext;
        
        // var pageBindingContext = tappedView.page.bindingContext,
        // fullItemsList = pageBindingContext.connections,
        // itemForTap = fullItemsList[args.index];        
        // console.log(JSON.stringify(x), function( key, value) {
        //     if( key == 'parent') { return value.id;}
        //     else {return value;}
        //   }
        // );
        // console.log(JSON.stringify(tappedView.page));
        // console.log(Object.getOwnPropertyNames(tappedItem));
        this.routerExtensions.navigate(["detalleProducto", tappedItem.nombre], {
            transition: {
                name: "fade"
            }
        });
    }

    doLater(fn) {setTimeout(fn, 500);}

    onCategoriaTap(e){
        dialogs.action("Elige una categoria para el producto", "Cancelar!", ["Fruta", "Verdura", "Conserva"])
            .then((result) => {
                console.log("resultado: " + result);   
                console.log("e: " + e.object.id);   
                this.productos.editar(e.object.id.replace("btn-",""), result);
                let toastOptions: Toast.ToastOptions = {text: "Editado correctamente", duration: Toast.DURATION.SHORT};
                this.doLater(() => Toast.show(toastOptions));
                // if (result == "Fruta") {
                // } else if (result == "Verdura") {
                // } else if (result == "Conserva") {
                // }
            });
 
        // dialogs.alert({
        //     title: "Titulo 1",
        //     message: "mje 1",
        //     okButtonText: "btn 1"
        // });
    }

    btnLoaded(args) {
        var btn = args.object;
        btn.android.setFocusable(false);
    };

    onPull(e) {        
        console.log(e);        
        const pullRefresh = e.object;        
        setTimeout(() => {            
            pullRefresh.refreshing = false;  
            this.productos.agregar("Papaya");      
        }, 2000); 
    } 

    buscarConFiltro(s:string){
        this.productos.buscar(s).then((r: any) => {
            // console.log(r);
            console.log("resultados buscarAhora " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });

        // this.resultados = this.productos.buscar().filter((x) => x.nombre.indexOf(s) >= 0);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"), 
            duration: 500, 
            delay:70
        }).then(() => layout.animate({
            backgroundColor: new Color("white"), 
            duration: 500, 
            delay:150
        }));

        // const icono = <View>this.icono.nativeElement;
        // icono.animate({
        //     rotate: 360, 
        //     duration: 800, 
        //     delay:70            
        // });
    }

    // onLongPress(s){
    //     console.log(s);
    // }

    onFavTap(e) {
        //   console.log(args);
       console.log(e.object.id);
       this.productos.favorito(e.object.id.replace("fav-",""));
      
    }

    onLongPress(s): void {
        console.log('s', s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso.");
    }

    // onLongPress(args: GestureEventData){
    //     // console.log("Object that triggered the event: " + args.object);
    //     console.log(args.view.id);

    //     // console.log(args.view.parentNode);
    //     // console.log("Event name: " + args.eventName);   
        
    //     const img = <Image>args.object;
    //     img.rotate = 0;
    //     img.animate({
    //         scale: { x:2, y: 2},
    //         duration: 500
    //     }).then(() => img.animate({
    //         scale: { x:1, y: 1},
    //         duration: 500            
    //     })).then(() =>
    //         dialogs.action("¿Que acción deseas realizar?", "Cancelar", ["Borrar", "Archivar"])
    //             .then((result) => {
    //                 console.log("resultado: " + result);  
    //                 this.doLater(() =>  {
    //                     if (result == "Borrar") {
    //                         this.productos.eliminar(args.view.id.replace("img-",""));
    //                     } else if (result == "Archivar") {
    //                         this.productos.archivar(args.view.id.replace("img-",""));
    //                     }
    //                     this.buscarConFiltro(args.view.id.replace("img-",""));
    //                 })
    //             }));
    // }
}
