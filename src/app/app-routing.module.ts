import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: () => import("~/app/home/home.module").then((m) => m.HomeModule) },
    { path: "browse", loadChildren: () => import("~/app/browse/browse.module").then((m) => m.BrowseModule) },
    { path: "search", loadChildren: () => import("~/app/search/search.module").then((m) => m.SearchModule) },
    { path: "productos", loadChildren: () => import("~/app/productos/listaProductos/listaProductos.module").then((m) => m.ListaProductosModule) },
    { path: "detalleProducto/:par", loadChildren: () => import("~/app/productos/detalleProductos/detalleProductos.module").then((m) => m.DetalleProductosModule) },
    { path: "clientes", loadChildren: () => import("~/app/clientes/clientes.module").then((m) => m.ClientesModule) },
    { path: "facturacion", loadChildren: () => import("~/app/facturacion/facturacion.module").then((m) => m.FacturacionModule) },
    { path: "featured", loadChildren: () => import("~/app/featured/featured.module").then((m) => m.FeaturedModule) },
    { path: "settings", loadChildren: () => import("~/app/settings/settings.module").then((m) => m.SettingsModule) },
    { path: "configVars", loadChildren: () => import("~/app/settings/configVars/configVars.module").then((m) => m.ConfigVarsModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
