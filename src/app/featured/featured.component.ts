import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ProductosService } from "../domain/productos.service";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { ComprarAction } from "../domain/productos-state.model";
import { Producto } from "../domain/producto.model";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {

    favoritos: Array<string>;
        
    constructor(private productos: ProductosService, private store: Store<AppState>) {
        this.favoritos = productos.favoritosGet(); 
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    btnLoaded(args) {
        var btn = args.object;
        btn.android.setFocusable(false);
    }

    onComprarTap(e):void{
        console.log(e.object.id);
        this.store.dispatch(new ComprarAction(new Producto(e.object.id.replace("com-",""))));
    }    
 
}
