import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { ClientesComponent } from './clientes.component';
// import { FacturacionComponent } from '../facturacion/facturacion.component';
import { ClientesRoutingModule } from './clientes-routing.module';


@NgModule({
  declarations: [ClientesComponent],
  imports: [
    NativeScriptCommonModule, ClientesRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ClientesModule { }
