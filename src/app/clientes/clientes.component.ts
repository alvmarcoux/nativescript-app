import { Component, OnInit } from '@angular/core';
import { ImageSource } from "tns-core-modules/image-source";
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import * as SocialShare from "nativescript-social-share";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
 
export class ClientesComponent implements OnInit {

  constructor(private routerExtensions: RouterExtensions) { }

  ngOnInit(): void {
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  // onNavItemTap(navItemRoute: string): void {
  //   this.routerExtensions.navigate([navItemRoute], {
  //       transition: {
  //           name: "fade"
  //       }
  //   });

  //   const sideDrawer = <RadSideDrawer>app.getRootView();
  //   sideDrawer.closeDrawer();
  // }
  onFactButtonTap(): void {
    this.routerExtensions.navigate(["facturacion"], {
      transition: {
          name: "fade"
      }
    });
    // const sideDrawer = <RadSideDrawer>app.getRootView();
    // sideDrawer.showDrawer();
  }


  onImgTap(s): void {
    console.log('comp im');
    let image = ImageSource.fromResource("icon").then((img) => SocialShare.shareImage(img,"Compartida desde NativeScript App."));
  }     
}
