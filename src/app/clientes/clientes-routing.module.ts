import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { ClientesComponent } from './clientes.component';
// import { FacturacionComponent } from '../facturacion/facturacion.component';

const routes: Routes = [
  { path: "", component: ClientesComponent },
  // { path: "../facturacion/", component: FacturacionComponent }
];

@NgModule({
  declarations: [],
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class ClientesRoutingModule { }
