import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);

@Component({
    selector: "Browse",
    moduleId: module.id,
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    // @ViewChild("MapView") mapView: ElementRef;
    gmaps = require("nativescript-google-maps-sdk");
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onMapReady(args): void {
        console.log("Map Ready");
        let mapView = args.object;
        let marker= new this.gmaps.Marker();
        marker.position = this.gmaps.Position.positionFromLatLng(-34.6037, -58.3817);
        marker.snippet = "Argentina"; 
        marker.userData = {index: 1};
        marker.title = "Buenos Aires";
        mapView.addMarker(marker);
    }
}
