import { Directive, Input } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
  selector: "[sololetras]",
  providers: [{provide: NG_VALIDATORS, useExisting: SoloLetrasDirective, multi: true}]
})
export class SoloLetrasDirective implements Validator {

  @Input() sololetras: string;

  constructor() {
    //
  }

  validate(control: AbstractControl): {[key: string]: any} {
      return !control.value || control.value.match(/^[a-z]*$/i) ? null : { sololetras: true };
  }
}