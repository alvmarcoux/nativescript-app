var productos_state_model = require("~/app/domain/productos-state.model");
console.log('entra a pruebas antes de describe');

describe("reducersProductos", function () {
    console.log('entra a pruebas');
    it("should reduce init data", function () {
        // setup
        var prevState = productos_state_model.initializeProductosState();
        var action = new productos_state_model.InitMyDataAction(["producto 1", "producto 2"]);
        // action
        var newState = productos_state_model.reducersProductos(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual("producto 1");
    });
    it("should reduce new item added", function () {
        var prevState = productos_state_model.initializeProductosState();
        var action = new productos_state_model.NuevoProductoAction(new productos_state_model.Producto("producto 3"));
        var newState = productos_state_model.reducersProductos(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual("producto 3");
    });
    it("should reduce sugerir producto", function () {
        var prevState = productos_state_model.initializeProductosState();
        var action = new productos_state_model.SugerirAction(new productos_state_model.Producto("producto sugerido"));
        var newState = productos_state_model.reducersProductos(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual("producto sugerido");
        expect(newState.items[0].sugerida.nombre).toEqual("producto sugerido");
    });
    it("should reduce comprar producto", function () {
        var prevState = productos_state_model.initializeProductosState();
        var action = new productos_state_model.ComprarAction(new productos_state_model.Producto("producto comprado"));
        var newState = productos_state_model.reducersProductos(prevState, action);
        expect(newState.itemsComprados.length).toEqual(1);
        expect(newState.itemsComprados[0].nombre).toEqual("producto comprado");
    });


});