import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { Producto } from "./producto.model";

// // ESTADO
// export class Producto {
//   constructor(public nombre: string, public categoria: string = "") { }
// }

// tslint:disable-next-line:interface-name
export interface ProductosState {
    // tslint:disable-next-line:array-type
    items: Producto[];
    itemsComprados: Producto[];
    sugerida: Producto;
}

export function initializeProductosState() {
  return {
    items: [],
    itemsComprados: [],
    sugerida: null
  };
}

// ACCIONES
export enum ProductosActionTypes {
  INIT_MY_DATA = "[Productos] Init My Data",
  NUEVO_PRODUCTO = "[Productos] Nuevo",
  SUGERIR_PRODUCTO = "[Productos] Sugerir", 
  COMPRAR_PRODUCTO = "[Productos] Comprar"
}

// tslint:disable-next-line:max-classes-per-file
export class InitMyDataAction implements Action {
  type = ProductosActionTypes.INIT_MY_DATA;
  constructor(public nombres: Array<string>) {}
}

// tslint:disable-next-line:max-classes-per-file
export class NuevoProductoAction implements Action {
  type = ProductosActionTypes.NUEVO_PRODUCTO;
  constructor(public Producto: Producto) {}
}

// tslint:disable-next-line:max-classes-per-file
export class SugerirAction implements Action {
  type = ProductosActionTypes.SUGERIR_PRODUCTO;
  constructor(public Producto: Producto) {}
}

export class ComprarAction implements Action {
  type = ProductosActionTypes.COMPRAR_PRODUCTO;
  constructor(public Producto: Producto) {}
}

export type ProductosActions = NuevoProductoAction | InitMyDataAction | SugerirAction | ComprarAction;

// REDUCERS
export function reducersProductos(
  state: ProductosState,
  action: ProductosActions
): ProductosState {
  switch (action.type) {
    case ProductosActionTypes.INIT_MY_DATA: {
      const titulares: Array<string> = (action as InitMyDataAction).nombres;

      return {
          ...state,
          items: titulares.map((t) => new Producto(t))
        };
    }
    case ProductosActionTypes.NUEVO_PRODUCTO: {
      return {
          ...state,
          items: [...state.items, (action as NuevoProductoAction).Producto ]
        };
    }
    case ProductosActionTypes.SUGERIR_PRODUCTO: {
      return {
          ...state,
          sugerida: (action as SugerirAction).Producto
        };
    }
    case ProductosActionTypes.COMPRAR_PRODUCTO: {
      console.log('redux comprar', (action as ComprarAction).Producto.nombre)
      return {
          ...state,
          itemsComprados: [...state.itemsComprados, (action as ComprarAction).Producto ]
        };
    }
  }

  return state;
}

// EFFECTS
// tslint:disable-next-line:max-classes-per-file
@Injectable()
export class ProductosEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(ProductosActionTypes.NUEVO_PRODUCTO),
    map((action: NuevoProductoAction) => new SugerirAction(action.Producto))
  );

  constructor(private actions$: Actions) {}
}

