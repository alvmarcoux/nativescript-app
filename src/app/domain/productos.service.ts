import { Injectable } from "@angular/core";
import { Producto } from "./producto.model";
import { getJSON, request} from "tns-core-modules/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class ProductosService {
    //Variable de configuración de Ngrok
    api: string = "https://5199f553ef90.ngrok.io";
    //api: string = "http://localhost:3000";

    private productos: Array<Producto> = [];
    private productosArchivados: Array<Producto> = [];

    constructor() {
        this.productos = [new Producto("Platano"),
            new Producto("Naranja"),
            new Producto("Higo"),
            new Producto("Datil"),
            new Producto("Guayaba"),
            new Producto("Manzana"),
            new Producto("Tomate"),
            new Producto("Mermelada"),
            new Producto("Pera"),
            new Producto("Calabaza")]

        this.getDb((db) => {
            // console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
            db.each("select * from favoritos",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Favoritos filas totales: ", totales));
            }, () => console.log("error on getDB")); 
    }

    // agregar(s:Producto) {
    //     this.productos.push(s);
    // }

    agregar(s:string) {
        this.productos.push(new Producto(s));
    }    

    eliminar(nombre:string) {
        // const index = this.productos.indexOf()
        const index = this.productos.findIndex(p => p.nombre === nombre);
        if (index > -1) {
            this.productos.splice(index, 1);
        }
    }

    archivar(nombre:string) {
        this.productosArchivados.push(new Producto(nombre));
        this.eliminar(nombre);
    }
    
    editar(nombre:string, categoria:string){
        let prod = this.productos.find(p => p.nombre === nombre);
        prod.categoria = categoria;
    }

    buscar(s:string) {
        // return this.productos; 
        return getJSON(this.api + "/get?q=" + s)
    }

    getDb(fnOk, fnError) {
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
              console.error("Error al abrir db!", err);
            } else {
                let errDB = false;
            //   console.log("Está la db abierta: ", db.isOpen() ? "Si" : "No");
            //   db.execSQL("delete from favoritos; delete from sqlite_sequence where name='favoritos';");
              db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                .then((id) => {
                    console.log("CREATE TABLE OK");
                }, (error) => {
                    console.log("CREATE TABLE ERROR", error);
                    fnError(error);
                    errDB = true;
                });
              db.execSQL("CREATE TABLE IF NOT EXISTS favoritos (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                .then((id) => {
                    console.log("CREATE TABLE OK");
                }, (error) => {
                    console.log("CREATE TABLE ERROR", error);
                    fnError(error);
                    errDB = true;
                }); 
                if (!errDB)
                    fnOk(db);               
            }
        });
    }

    favorito(s: string) {
        this.getDb((db) => {
            db.execSQL("insert into favoritos (texto) values (?)", [s],
            (err, id) => console.log("nuevo id: ", id));
        }, () => console.log("error on getDB"));    
    }  

    favoritosGet() {
        let favoritos = [];        
        this.getDb((db) => {
            db.each("select texto from favoritos",
                (err, fila) => favoritos.push(fila),
                (err, totales) => console.log("Favoritos filas totales: ", totales));
            }, () => console.log("error on getDB")); 
        return favoritos;
    }
}